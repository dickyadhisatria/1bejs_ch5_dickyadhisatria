const express = require("express");
const router = express.Router();
const {
  createChannel,
  updateChannel,
  deleteChannel,
} = require("../controllers/channelController");

router.post("/createChannel", createChannel);
router.delete("/deleteChannel/:id", deleteChannel);
router.put("/updateChannel/:id", updateChannel);

module.exports = router;
