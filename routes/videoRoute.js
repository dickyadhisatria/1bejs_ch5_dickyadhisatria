const express = require("express");
const router = express.Router();
const {
  createNewVideo,
  getAllVideos,
  getVideoById,
  deleteVideoById,
} = require("../controllers/videoController");

router.post("/createVideo", createNewVideo);
router.get("/allVideos", getAllVideos);
router.get("/getVideoById/:id", getVideoById);
router.delete("/deleteVideoById/:id", deleteVideoById);

module.exports = router;
