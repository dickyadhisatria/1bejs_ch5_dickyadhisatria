const express = require("express");
const router = express.Router();
const userRoutes = require("./userRoute");
const channelRoutes = require("./channelRoute");
const videoRoutes = require("./videoRoute");

router.use("/user", userRoutes);
router.use("/channel", channelRoutes);
router.use("/video", videoRoutes);

module.exports = router;
