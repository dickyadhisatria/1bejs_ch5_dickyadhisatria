const routes = require("express").Router();
const {
  createUser,
  getAllUsers,
  loginUser,
  deleteUser,
} = require("../controllers/userController");

routes.post("/register", createUser);
routes.get("/users", getAllUsers);
routes.post("/login", loginUser);
routes.delete("/deleteuser/:id", deleteUser);

module.exports = routes;
