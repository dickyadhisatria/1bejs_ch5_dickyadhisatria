"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "videos",
      [
        {
          channel_id: 1,
          title: "How to use Node.js",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          channel_id: 2,
          title: "How to use React.js",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
