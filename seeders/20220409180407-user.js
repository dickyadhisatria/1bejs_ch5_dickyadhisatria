"use strict";
const bcrypt = require("bcrypt");

module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "users",
      [
        {
          username: "dickyadhisatria",
          password: await bcrypt.hash("dicky2uxgg", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          username: "mugeki",
          password: await bcrypt.hash("root", 10),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
