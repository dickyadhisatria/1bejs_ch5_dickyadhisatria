const express = require("express");
const router = require("./routes/indexRoute");
const app = express();
const port = 3000;
const morgan = require("morgan");
const basicAuth = require("express-basic-auth");
const helmet = require("helmet");
const bodyParser = require("body-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./api-doc/swagger-output.json");

app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(helmet());

// const basicAuthResponse = (req) => {
//   return req.auth
//     ? "Credentials " + req.auth.user + ":" + req.auth.password + " rejected"
//     : "Unauthorized";
// };

// app.use(
//   basicAuth({
//     users: {
//       dickyadhisatria: "dicky2uxgg",
//     },
//     unauthorizedResponse: basicAuthResponse,
//   })
// );

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/", router);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
