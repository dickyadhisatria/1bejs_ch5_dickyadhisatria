const { video: Video } = require("../models");
const { channel: Channel } = require("../models");

const createNewVideo = async (req, res) => {
  const { title, channel_id } = req.body;
  const video = new Video(
    {
      title,
      channel_id,
    },
    {
      include: [
        {
          model: Channel,
          as: "channel",
          attributes: ["id", "channel_name"],
        },
      ],
    }
  );
  try {
    const savedVideo = await video.save();
    res
      .status(200)
      .json({ message: "Video created successfully", video: savedVideo });
  } catch (err) {
    res.status(400).send(err);
  }
};

const getAllVideos = async (req, res) => {
  let { page, row } = req.query;
  page -= 1;
  try {
    const videos = await Video.findAll({
      attributes: ["id", "title", "channel_id"],
      offset: page,
      limit: row,
    });
    res.status(200).json({ message: "Videos found", videos: videos });
  } catch (err) {
    res.status(400).send(err);
  }
};

const getVideoById = async (req, res) => {
  try {
    const video = await Video.findOne({
      where: {
        id: req.params.id,
      },
      include: [
        {
          model: Channel,
          as: "channel",
          attributes: ["id", "channel_name"],
        },
      ],
    });
    res.status(200).json({ message: "Video found", video: video });
  } catch (err) {
    res.status(400).send(err);
  }
};

const deleteVideoById = async (req, res) => {
  try {
    const video = await Video.findOne({
      where: {
        id: req.params.id,
      },
    });
    if (!video) {
      res.status(400).send({ error: "Video not found" });
    }
    const deletedVideo = await video.destroy();
    res.status(200).json({
      message: `Video deleted successfully`,
      video: deletedVideo,
    });
  } catch (err) {
    res.status(400).send(err);
  }
};

module.exports = {
  createNewVideo,
  getAllVideos,
  getVideoById,
  deleteVideoById,
};
