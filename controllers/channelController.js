const { channel: Channel } = require("../models");
const { user: User } = require("../models");

const createChannel = async (req, res) => {
  const { channel_name, user_id } = req.body;
  const channel = new Channel(
    {
      channel_name,
      user_id,
    },
    {
      include: [{ model: User, as: "user", attributes: ["id", "username"] }],
    }
  );
  try {
    const savedChannel = await channel.save();
    res
      .status(200)
      .json({ message: "Channel created successfully", channel: savedChannel });
  } catch (err) {
    res.status(400).send(err);
  }
};

const updateChannel = async (req, res) => {
  const { channel_name, user_id } = req.body;
  try {
    const channel = await Channel.findOne({
      where: {
        id: req.params.id,
      },
    });
    if (!channel) {
      res.status(400).send({ error: "Channel not found" });
    }
    channel.channel_name = channel_name;
    channel.user_id = user_id;
    const savedChannel = await channel.save();
    res
      .status(200)
      .json({ message: "Channel updated successfully", channel: savedChannel });
  } catch (err) {
    res.status(400).send(err);
  }
};

const deleteChannel = async (req, res) => {
  try {
    const channel = await Channel.findOne({
      where: {
        id: req.params.id,
      },
    });
    if (!channel) {
      res.status(400).send({ error: "Channel not found" });
    }
    const deletedChannel = await channel.destroy();
    res.status(200).json({
      message: `Channel deleted successfully`,
      channel: deletedChannel,
    });
  } catch (err) {
    res.status(400).send(err);
  }
};

module.exports = {
  createChannel,
  updateChannel,
  deleteChannel,
};
