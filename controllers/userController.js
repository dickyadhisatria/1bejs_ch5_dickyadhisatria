const { user: User } = require("../models");
const bcrypt = require("bcrypt");

const createUser = async (req, res) => {
  const { username, password } = req.body;
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);
  const user = new User({
    username,
    password: hashedPassword,
  });
  try {
    const savedUser = await user.save();
    res.send({ message: "User created successfully", user: savedUser });
  } catch (err) {
    res.status(400).send(err);
  }
};

const getAllUsers = async (req, res) => {
  let { page, row } = req.query;
  page -= 1;
  try {
    const users = await User.findAll({
      attributes: ["id", "username"],
      offset: page,
      limit: row,
    });
    res.json({ message: "Users found", users: users });
  } catch (err) {
    res.status(400).send(err);
  }
};

const loginUser = async (req, res) => {
  const { username, password } = req.body;
  try {
    const user = await User.findOne({
      where: {
        username: username,
      },
    });
    if (!user) {
      res.status(400).send({ error: "User not found" });
    }
    const isMatch = await bcrypt.compare(password, user.password);
    if (!isMatch) {
      res.status(400).send({ error: "Invalid password" });
    }
    res
      .status(200)
      .json({ message: "User logged in successfully", data: user });
  } catch (err) {
    res.status(400).send(err);
  }
};

const deleteUser = async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        id: req.params.id,
      },
    });
    if (!user) {
      res.status(400).send({ error: "User not found" });
    }
    const deletedUser = await user.destroy();
    res.json({ message: `User deleted successfully`, user: deletedUser });
  } catch (err) {
    res.status(400).send(err);
  }
};

module.exports = {
  createUser,
  getAllUsers,
  loginUser,
  deleteUser,
};
